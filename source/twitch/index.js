import tmi from 'tmi.js';
import { Channel } from './channel';

export default class TwitchBot {
	constructor(instance, channels, logics = []) {
		this.instance = instance;
		this.config = instance.config;
		this.log = instance.log;

		this.channels = channels;
		this.logics = logics;

		this.tmi = {
			identity: {
				username: this.config.twitch.bot.username,
				password: this.config.twitch.bot.oauth,
			},
			channels: channels,
		};

		this.options = {
			channel: {
				name: this.config.twitch.channel.name,
				oauth: this.config.twitch.channel.oauth,
				id: this.config.twitch.channel.id,
			},
			app: {
				clientID: this.config.twitch.app.clientID,
				secret: this.config.twitch.app.secret,
				token: this.config.twitch.app.token,
			},
			api: {
				callbackURL: this.config.twitch.api.callbackURL,
				port: this.config.twitch.api.port,
				secret: this.config.twitch.api.secret,
				streamlabs: this.config.twitch.streamlabs.token,
			},
		};
	}

	initClient() {
		this.client = new tmi.client(this.tmi);

		this.client.on('message', this.onMessageHandler.bind(this));
		this.client.on('connected', this.onConnectedHandler.bind(this));

		this.client.connect();
	}

	initChannel() {
		console.log('Init channel...');

		if (this.options.channel.name) {
			this.channel = new Channel(this.options, this.tmi, this.instance);
			this.channel.init();
			this.channel.initChat();
			console.log('Success!');
		} else {
			console.log('Failed!');
		}
	}

	onMessageHandler(target, context, message, self) {
		if (self) {
			return;
		}
		const roomID = context['room-id']; // If data is a string, otherwise use .whatever

		//console.log('Context room ID: ', roomID); // Uncomment this line to figure out the room ID for desired chat-channel

		if (this.logics[roomID]) {
			this.logics[roomID].onMessage(target, context, message, self, this.client, this.instance);
		}
		// Filter rooms

		// If you want to communicate with Discord,
		// and you're only using one set of logic (inside of this block),
		// then use the getter from the instance instead.
		// this.instance.getDiscord().sendMessage(msg);
	}

	onConnectedHandler(address, port) {
		this.log.debug(`* Connected to: ${address}:${port}`);
	}
}
