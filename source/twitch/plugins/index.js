import fs from 'fs';
import path from 'path';

const context = {
	badge: 'badge-info',
	badgeInfoRaw: 'badge-info-raw',
	badges: 'badges',
	badgesRaw: 'badges-raw',
	channelID: 'room-id',
	client: 'client-nonce',
	color: 'color',
	colour: 'color',
	displayName: 'display-name',
	emotes: 'emotes',
	emotesRaw: 'emotes-raw',
	isFirstMsg: 'first-msg',
	flags: 'flags',
	id: 'id',
	msgType: 'message-type',
	mod: 'mod',
	sub: 'subscriber',
	ts: 'tmi-sent-ts',
	turbo: 'turbo',
	userID: 'user-id',
	userType: 'user-type',
	username: 'username',
};

const headers = {
	messageID: 'Twitch-Eventsub-Message-Id'.toLowerCase(),
	messageTS: 'Twitch-Eventsub-Message-Timestamp'.toLowerCase(),
	messageSignature: 'Twitch-Eventsub-Message-Signature'.toLowerCase(),
};

export function getContext() {
	return context;
}

export function getContextValue(val) {
	return context[val];
}

export function getHeaders() {
	return headers;
}

export function getHeaderValue(val) {
	return headers[val];
}

export function getPlugins() {
	const pluginDirectory = path.join(__dirname, './channels');
	const channels = [];
	const plugins = {};

	fs.readdirSync(pluginDirectory).forEach((file) => {
		if (!file.includes('.js')) {
			throw new Error(`Invalid file extension: Only .js files allowed in ${pluginDirectory}`);
			return;
		}

		const pluginResult = require(`${pluginDirectory}/${file}`);
		const plugin = new pluginResult.default();

		channels.push(plugin.getChannelName());
		plugins[plugin.getChannelID()] = plugin;
	});

	console.log(channels);
	console.log(plugins);

	return { channels: channels, plugins: plugins };
}
