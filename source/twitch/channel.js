import { EventEmitter } from 'events';
import { ApiClient } from '@twurple/api';
import { ClientCredentialsAuthProvider, StaticAuthProvider } from '@twurple/auth';
import { EventSubListener, DirectConnectionAdapter, ReverseProxyAdapter } from '@twurple/eventsub';

import { getContext, getContextValue } from './plugins';
import { NgrokAdapter } from '@twurple/eventsub-ngrok';

import axios from 'axios';
import tmi from 'tmi.js';

export class Channel extends EventEmitter {
	constructor(options, tmi, instance) {
		super();

		this.options = options;
		this.tmi = tmi;
		this.instance = instance;
		this.log = instance.log;

		this.apiClient;
		this.listener;

		this.eventSubscriber = [];
	}

	init() {
		//const authProvider = new StaticAuthProvider(this.options.app.clientID, this.options.app.token);
		const authProvider = new ClientCredentialsAuthProvider(this.options.app.clientID, this.options.app.secret);
		const apiClient = new ApiClient({ authProvider });
		apiClient.eventSub.deleteAllSubscriptions();
		this.apiClient = apiClient;
		//console.log('API Client: ', apiClient);

		/*
		const adapter = new DirectConnectionAdapter({
			hostName: '',
			sslCert: {
				key: '',
				cert: '',
			}
		});
		*/

		this.listener = new EventSubListener({
			apiClient,
			adapter: new NgrokAdapter(),
			secret: this.options.api.secret,
		});

		this.on('subscription', (id, name) => this.subscriptionEvent(id, name));
		this.on('subscription-end', (id, name) => this.subscriptionEndEvent(id, name));
		this.on('subscription-gift', (id, name) => this.subscriptionGiftEvent(id, name));
		this.on('follow', (id, name) => this.followEvent(id, name));
		this.on('redemption-add', (id, name) => this.redemptionAddEvent(id, name));
		this.on('redemption-update', (id, name) => this.redemptionUpdateEvent(id, name));

		this.start();
	}

	initChat() {
		this.client = new tmi.client(this.tmi);

		this.client.on('message', this.onMessageHandler.bind(this));
		this.client.on('connected', this.onConnectedHandler.bind(this));

		this.client.connect();
	}

	onMessageHandler(target, context, message, self) {
		if (self) {
			return;
		}

		//console.log('Context: ', context);

		const commandName = message.trim();

		if (commandName.charAt(0) === '!' && commandName.includes('!link twitch')) {
			const key = commandName.replace('!link twitch', '').trim();

			console.log('Key: ', key);
			const userID = context['user-id'];
			const name = context['display-name'];

			this.apiCall('/twitch/link', { key, name, userID }, { target, message: 'Linked!' });
		}
	}

	onConnectedHandler(address, port) {
		this.log.debug(`* Connected to: ${address}:${port}`);
	}

	async cleanUp() {
		//await Promise.all([...this.eventSubscriber.map((sub) => sub.stop()), this.listener.unlisten()]);
	}

	async start() {
		await this.cleanUp();

		const stream = await this.apiClient.streams.getStreamByUserName(this.options.channel.name);
		const lastGame = stream?.gameName;
		//console.log('Last game: ', lastGame);

		await this.listener.listen();

		const channel = await this.apiClient.users.getUserByName(this.options.channel.name);

		if (!channel) {
			throw new Error('Channel not found.');
		}

		this.eventSubscriber.push(
			await this.listener.subscribeToStreamOnlineEvents(channel, (event) => {
				console.log(`${event.broadcasterDisplayName} just went live!`);
			}),
		);

		this.eventSubscriber.push(
			await this.listener.subscribeToChannelRedemptionAddEvents(channel, (event) => {
				const id = event.id;
				const name = event.userDisplayName;
				const userID = event.userId;
				const rewardID = event.rewardId;
				const rewardTitle = event.rewardTitle;

				this.emit('redemption-add', { id, name, userID, rewardID, rewardTitle });
			}),
		);

		this.eventSubscriber.push(
			await this.listener.subscribeToChannelRedemptionUpdateEvents(channel, (event) => {
				const id = event.userId;
				const name = event.userDisplayName;

				this.emit('redemption-update', { id, name });
			}),
		);

		this.eventSubscriber.push(
			await this.listener.subscribeToChannelFollowEvents(channel, (event) => {
				const id = event.userId;
				const name = event.userDisplayName;

				this.emit('follow', { id, name });
			}),
		);

		this.eventSubscriber.push(
			await this.listener.subscribeToChannelSubscriptionEvents(channel, (event) => {
				const id = event.userId;
				const name = event.userDisplayName;

				this.emit('subscription', { id, name });
			}),
		);

		this.eventSubscriber.push(
			await this.listener.subscribeToChannelSubscriptionEndEvents(channel, (event) => {
				const id = event.userId;
				const name = event.userDisplayName;

				this.emit('subscription-end', { id, name });
			}),
		);

		this.eventSubscriber.push(
			await this.listener.subscribeToChannelSubscriptionGiftEvents(channel, (event) => {
				const id = event.userId;
				const name = event.userDisplayName;

				this.emit('subscription-gift', { id, name });
			}),
		);
	}

	apiCall(endpoint, data, chat = { target: undefined, message: 'default_bot_message' }) {
		const host = 'http://localhost:8194/api/';

		const request = axios.create({
			baseURL: host,
			timeout: 10000,
			headers: {},
		});

		request
			.post(endpoint, data)
			.then((res) => {
				if (res.data.status === 200 && chat.target) {
					this.client.say(chat.target, chat.message);
				} else {
					this.client.say(chat.target, res.data.message);
				}
				//console.log('Response: ', res.data);
			})
			.catch((err) => {
				console.log('ERROR: ', err);
				if (chat.target) {
					this.client.say(chat.target, 'ERROR!');
				}
			});
	}

	followEvent({ id, name }) {
		console.log('Event::follow');
		console.log('Name: ', name);
	}

	subscriptionEvent({ id, name }) {
		console.log('Event::subscription');
		console.log('Name: ', name);
	}

	subscriptionEndEvent({ id, name }) {
		console.log('Event::subscription-end');
		console.log('Name: ', name);
	}

	subscriptionGiftEvent({ id, name }) {
		console.log('Event::subscription-gift');
		console.log('Name: ', name);
	}

	redemptionAddEvent({ id, name, userID, rewardID, rewardTitle }) {
		console.log('Event::redemption-add');
		console.log('ID: ', id);
		console.log('Name: ', name);
		console.log('User ID: ', userID);
		console.log('Reward ID: ', rewardID);
		console.log('Reward Title: ', rewardTitle);

		this.apiCall('/twitch/redemption', { id, name, userID, rewardID, rewardTitle });
	}

	redemptionUpdateEvent({ id, name }) {
		console.log('Event::redemeption-update');
		console.log('Name: ', name);
	}
}
