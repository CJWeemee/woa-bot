/**
 * Emoji: https://getemoji.com/
 * Guide: https://discordjs.guide/popular-topics/reactions.html#unicode-emojis
 * Multiple guilds: https://stackoverflow.com/questions/63240620/how-to-store-data-for-a-user-across-servers-discord
 */

import { Client } from 'discord.js';

const STALK_THIS_ONE = '113719234789507076'; // author.id

export default class DiscordBot {
	constructor(instance, intents) {
		this.instance = instance;

		this.config = instance.config;
		this.log = instance.log;
		this.client = new Client({
			intents: intents,
		});
	}

	initClient() {
		this.client.once('ready', () => {
			this.log.debug('Discord bot client is ready!');
		});

		this.client.login(this.config.discord.token);

		this.client.on('interactionCreate', (interaction) => {
			this.log.debug('on.interactionCreate');
		});

		this.client.on('messageCreate', (message) => {
			this.log.debug('on.messageCreate');

			//if (message.author.id === STALK_THIS_ONE) {
			message.react('🦒');
			//}
		});
	}

	sendMessage(msg) {
		console.log('Message to send (from Twitch): ', msg);

		const channel = this.client.channels.cache.get('868187685448196166');
		channel.send(`[Twitch]: ${msg}`);
	}
}
