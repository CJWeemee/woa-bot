// Main file
import dotenv from 'dotenv';
import { buildConfig } from 'libs/config';
import DiscordBot from './discord';
import Logger from 'libs/logger';
import chalk from 'chalk';

import { Intents } from 'discord.js';
import TwitchBot from './twitch';

import { getPlugins } from './twitch/plugins';

dotenv.config();
const config = buildConfig();

const year = new Date().getFullYear();
let month = new Date().getMonth() + 1;

if (month < 10) {
	month = '0' + month;
}

const day = new Date().getUTCDate();
const dateString = year + '-' + month + '-' + day;

console.log(chalk.yellow(`${dateString}`));

const log = new Logger({
	level: process.env.NODE_ENV === 'development' ? 'debug' : 'error',
	debugFile: `${__dirname}/../logs/${dateString}_debug.log`,
	infoFile: `${__dirname}/../logs/${dateString}_info.log`,
	warnFile: `${__dirname}/../logs/${dateString}_warn.log`,
	errorFile: `${__dirname}/../logs/${dateString}_error.log`,
	adminFile: `${__dirname}/../logs/${dateString}_admin.log`,
});

class Main {
	constructor() {
		this.log = log;
		this.config = config;
	}

	init() {
		this.Discord = new DiscordBot(this, [
			Intents.FLAGS.GUILDS,
			Intents.FLAGS.GUILD_MESSAGES,
			Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
		]);

		const TwitchPlugins = getPlugins();

		this.Twitch = new TwitchBot(this, [this.config.twitch.channel.name]);

		this.Discord.initClient();
		//this.Twitch.initClient();
		this.Twitch.initChannel();
	}

	getDiscord() {
		return this.Discord;
	}

	sendMessageToDiscord(msg) {
		this.Discord.sendMessage(msg);
	}
}

(() => {
	new Main().init();
})();
