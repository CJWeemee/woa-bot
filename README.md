# WoA Bot

The link between Twitch & Discord to World of Atoms. This will be marked as private, since there's lots of special stuff I want to keep for myself. I will open up a new public repository.

---
## Commands

Creates a new channel file inside of "plugins/channels" from a template
{Required}: name=
{Optional}: id=

npm run add-twitch-channel name=channelName id=channelID

---
## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

---
## Some settings
### Generate OAuth token
---
  Get your token at [twitchapps](https://twitchapps.com/tmi/)

---

### TMI
  - identity
    - username: Twitch account that will act as a bot
    - password: OAuth for that Twitch account
  - channels: String array of channels

---
### Options (Twitch & Streamlabs)
- channel
  - name: Name of the channel
  - oauth: OAuth for that Twitch channel
  - id: Twitch channel ID
- app
  - clientID: The client ID of the registered [Twitch App](https://dev.twitch.tv/console/apps)
  - secret: The secret from the registered Twitch App
  - token: The token generated from authorizing your own Twitch App with your Twitch channel account
- api
  - callbackURL: The URL Twitch will reach your bot. This needs to be HTTPS and use port 443. Manual networking configs (proxy) will work too. If localhost, use [ngrok](https://ngrok.com/download)
  - port: The port for the URL above (ignore if using ngrok or standard HTTPS)
  - secret: Special HMAC key for data transaction between Twitch and the bot
  - streamlabs: The token you get from [Streamlabs](https://streamlabs.com/dashboard#/settings/api-settings) under "API Tokens" > "Your Socket API Token"

---