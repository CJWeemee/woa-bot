import fs from 'fs';
import path from 'path';

export function dotEnv(key, defaultValue, split = null) {
	let configValue = defaultValue;

	if (typeof process.env[key] !== 'undefined' && process.env[key] !== '') {
		configValue = process.env[key];
	}

	return split && typeof configValue === 'string' ? configValue.split(split) : configValue;
}

export function buildConfig() {
	const configDirectory = path.join(__dirname, '../libs/', 'conf');
	const config = {};

	fs.readdirSync(configDirectory).forEach((file) => {
		if (!file.includes('.js')) {
			console.log('Can not find file');
			return;
		}

		const configResult = require(`${configDirectory}/${file}`);
		config[file.toLowerCase().replace('.js', '')] = configResult.default;
	});

	return config;
}
