import { getContext, getContextValue } from '../index';

export default class Plugin {
	constructor() {
		this.channelID = null;
		this.channelName = null;
	}

	getChannelName() {
		return this.channelName;
	}

	getChannelID() {
		return this.channelID;
	}

	onMessage(target, context, message, self, client, instance) {
		if (self) {
			return;
		}

		const commandName = message.trim();

		// Make sure message starts with !
		if (commandName.charAt(0) === '!') {
			// If our command is !example
			if (commandName.toLowerCase().includes('!example')) {
				const msg = commandName.replace('!example', '').trim();
				client.say(target, `Example: ${msg}`);
			}
		}
	}
}
