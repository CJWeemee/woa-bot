import path from 'path';
import { execSync } from 'child_process';
import fs from 'fs';
import Logger from '../logger';

const [, , ...args] = process.argv;
let name, id;

const pathToRoot = path.join(__dirname, '../..');
const log = new Logger({
	level: 'info',
	debugFile: `${pathToRoot}/logs/debug.log`,
	infoFile: `${pathToRoot}/logs/info.log`,
	warnFile: `${pathToRoot}/logs/warn.log`,
	errorFile: `${pathToRoot}/logs/error.log`,
});

if (!args[0]) {
	throw log.error('Found no arguments');
}

if (!args[0].includes('name=')) {
	throw log.error('First argument must be name=channelName');
}

if (args[1] && !args[1].includes('id=')) {
	throw log.error('Second argument must be id=channelID');
}

name = args[0].replace('name=', '');

if (args[1]) {
	id = args[1].replace('id=', '');
}

const fileName = `${pathToRoot}/source/twitch/plugins/channels/${name}.js`;

if (!fs.existsSync(fileName)) {
	log.info('Name is unique, creating...');

	execSync(`cp -n ${pathToRoot}/libs/templates/twitchChannel.js ${fileName}`);

	log.info('File created!');

	try {
		log.info('Injecting arguments into file...');

		const filePath = fileName;
		let confData = fs.readFileSync(filePath, { encoding: 'utf8' });

		if (id) {
			confData = confData.replace('this.channelID = null', `this.channelID = '${id}'`);
		}

		confData = confData.replace('this.channelName = null', `this.channelName = '${name}'`);

		fs.writeFileSync(filePath, confData);
	} catch (err) {
		throw log.error('Could not inject arguments into file, ERROR: ', err);
	}
} else {
	log.warn('A channel with that name already exists!');
}
