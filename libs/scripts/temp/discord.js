import { dotEnv } from '../config';

export default {
	token: dotEnv('DISCORD_TOKEN', ''),
	appID: dotEnv('DISCORD_APP_ID', ''),
	publicKey: dotEnv('DISCORD_PUBLIC_KEY', ''),
};
