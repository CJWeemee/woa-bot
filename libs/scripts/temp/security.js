import { dotEnv } from '../config';

export default {
	encryption: {
		ivLength: dotEnv('SECURITY_ENCRYPTION_IV_LENGTH', 1),
		algo: dotEnv('SECURITY_ENCRYPTION_ALGORITHM', ''),
		secret: dotEnv('SECURITY_ENCRYPTION_SECRET', ''),
	},
	certificate: {
		cert: dotEnv('SECURITY_CERTIFICATE_CERT', ''),
		key: dotEnv('SECURITY_CERTIFICATE_KEY', ''),
		ca: dotEnv('SECURITY_CERTIFICATE_CA', ''),
	},
};
