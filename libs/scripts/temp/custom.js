import { dotEnv } from '../config';

export default {
	customEnv: dotEnv('MY_CUSTOM_ENV', 'This is custom'),
};
