import { dotEnv } from '../config';

export default {
	channel: {
		name: dotEnv('TWITCH_CHANNEL_NAME', ''),
		oauth: dotEnv('TWITCH_CHANNEL_OAUTH', ''),
		id: dotEnv('TWITCH_CHANNEL_ID', ''),
	},
	app: {
		clientID: dotEnv('TWITCH_APP_CLIENT_ID', ''),
		secret: dotEnv('TWITCH_APP_SECRET', ''),
		token: dotEnv('TWITCH_APP_TOKEN', ''),
	},
	api: {
		callbackURL: dotEnv('TWITCH_API_CALLBACK_URL', ''),
		port: dotEnv('TWITCH_API_PORT', ''),
		secret: dotEnv('TWITCH_API_SECRET', 'this7is7a7secret7change7please'),
	},
	bot: {
		username: dotEnv('TWITCH_BOT_USERNAME', ''),
		oauth: dotEnv('TWITCH_BOT_OAUTH', ''),
	},
	streamlabs: {
		token: dotEnv('STREAMLABS_SOCKET_TOKEN', undefined),
	},
};
